const DOMAIN  = ((process.env.NODE_ENV === 'production')? 'http://h2m-solution.com': 'http://192.168.1.177')
const API_URL = DOMAIN  + '/api/v1'
export const appConfig = {
  DOMEN: DOMAIN,
  API_URL: API_URL,
  URL:{
    login: API_URL + '/login',
    user: API_URL + '/admin/user',
    area: API_URL + '/area',
    areaDeviceGroup: API_URL + '/area/device-group',
    areaDevice: API_URL + '/area/device',
    areasUser: API_URL + '/area/user',
    deviceGroup: API_URL + '/device-group',
    device: API_URL + '/device',
    deviceManualControlOnOfSet: API_URL + '/device/manual-on-off',
    deviceOnOffSet: API_URL + '/device/on-off',
    areaConfigureAction: API_URL + '/area/configure-action',
    configureAction: API_URL + '/configure-action'

  },
  PUSHER: {
    appId: '370133',
    key: 'b85706003dbbc40a2673',
    secret: 'e4593bef01b5f4d2e005',
    cluster: 'eu',
    encrypted: true
  }
};
