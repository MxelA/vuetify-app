/**
 * Created by Aleksandar Milic on 5/23/17.
 */
// import router from './router/router'

import appConfig from './auth'

export default {
  error (Vue, error, callback) {
    const statusCode = error.status

    switch (true) {
      case (statusCode === 201 || statusCode === 202):
        Vue.$store.dispatch('alert', {
          type: 'success',
          message: error.statusText,
          timeout: 5000
        })

        break
      case (statusCode === 400):

        if (error.body.error === 'token_not_provided') {
          Vue.$store.dispatch('alert', {
            type: 'danger',
            message: 'Code:' + statusCode + ' - ' + error.body.error,
            timeout: 5000
          })
          appConfig.logout(Vue)
        }
        break

      case (statusCode === 401):

        if (error.body.error === 'token_expired') {
          Vue.$store.dispatch('alert', {
            type: 'danger',
            message: 'Code:' + statusCode + ' - ' + error.body.error,
            timeout: 5000
          })
          appConfig.logout(Vue)
        }
        break

      case (statusCode === 422):
        Vue.$store.dispatch('alert', {
          type: 'danger',
          message: error.data.message,
          timeout: 10000
        })

        Vue.$store.dispatch('alert', {
          type: 'danger',
          message: Object.values(error.data.errors).join(','),
          timeout: 10000
        })
        break

      default:
        Vue.$store.dispatch('alert', {
          type: 'danger',
          message: 'Code:' + statusCode + ' - ' + error.statusText,
          timeout: 5000
        })
    }

    if (callback) {
      callback(error)
    }
  }
}
