/**
 * Created by Aleksandar Milic on 5/23/17.
 */
import {appConfig} from '../../config/app'
import router from '../router/index'

export default {

  // Send a request to the login URL and save the returned JWT
  login (vue, credentials, callback) {
    vue.$http.post(appConfig.URL.login, credentials).then(response => {
      sessionStorage.setItem(appConfig.API_URL + 'userToken', response.data.token)
      sessionStorage.setItem(appConfig.API_URL + 'userName', response.data.user.name)
      sessionStorage.setItem(appConfig.API_URL + 'userRole', response.data.user.role)
      sessionStorage.setItem(appConfig.API_URL + 'userId', response.data.user.id)

      vue.$store.state.authUser.id = response.data.user.id
      vue.$store.state.authUser.name = response.data.user.name
      vue.$store.state.authUser.token = response.data.token
      vue.$store.state.authUser.role = response.data.user.role
      vue.$store.state.authUser.authenticated = true

      callback(response)
    }, error => {
      callback(error)
    })
  },
  // To log out, we just need to remove the token
  logout (vue) {
    sessionStorage.removeItem(appConfig.API_URL + 'userToken')
    sessionStorage.removeItem(appConfig.API_URL + 'userName')
    sessionStorage.removeItem(appConfig.API_URL + 'userRole')
    sessionStorage.removeItem(appConfig.API_URL + 'userId')

    vue.$store.state.authUser.id = null
    vue.$store.state.authUser.name = 'Guest'
    vue.$store.state.authUser.token = null
    vue.$store.state.authUser.role = null
    vue.$store.state.authUser.authenticated = false

    router.push({name: 'Hello'})
  },
  getToken () {
    return sessionStorage.getItem(appConfig.API_URL + 'userToken')
  }
}
