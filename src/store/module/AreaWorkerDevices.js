import {appConfig} from '../../../config/app'
import Vue from 'vue'

export const areaWorkerDevicesModule = {
  state: {
    areaWorkerDeviceCollection: {}
  },
  getters: {
    areaWorkerDeviceCollection (state) {
      return state.areaWorkerDeviceCollection
    }
  },
  mutations: {
    setAreaWorkerDeviceCollection (state, areaWorkerDeviceCollection) {
      state.areaWorkerDeviceCollection = areaWorkerDeviceCollection
    }
  },
  actions: {
    fetchAreaWorkerDeviceCollection (context, data) {
      const url = appConfig.URL.areaDevice + '/worker/' + data.areaId + '' + (data.getParams ? data.getParams : '')

      Vue.http.get(url).then(response => {
        context.commit('setAreaWorkerDeviceCollection', response.data)
      })
    }
  }
}
