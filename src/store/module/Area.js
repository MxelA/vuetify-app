import {appConfig} from '../../../config/app'
import Vue from 'vue'

export const areaModule = {
  state: {
    area: {
      additional_data: {}
    },
    areaCollection: {}
  },
  getters: {
    getArea (state) {
      return state.area
    },
    areaCollection (state) {
      return state.areaCollection
    }
  },
  mutations: {
    setArea (state, area) {
      state.area = area
    },
    setAreaCollection (state, areaCollection) {
      state.areaCollection = areaCollection
    }
  },
  actions: {
    fetchArea (context, areaId) {
      context.dispatch('progressBar', true)

      Vue.http.get(appConfig.URL.area + '/' + areaId).then(response => {
        context.commit('setArea', response.data.data)
        context.dispatch('progressBar', false)
      }, () => {
        context.dispatch('progressBar', false)
      })
    },

    fetchAreaCollection (context, data) {
      let url = appConfig.URL.areasUser + '/' + data.userId + '' + (data.getParams ? data.getParams : '')
      Vue.http.get(url).then(response => {
        context.commit('setAreaCollection', response.data)
      })
    }
  }
}
