export const devicesModule = {
  state: {
    devicePublishers: [],
    publisherDevices: [],
    workerDevices: []
  },
  getters: {
    getPublisherDevices (state) {
      return state.publisherDevices
    },
    getWorkerDevices (state) {
      return state.workerDevices
    },
    devicePublishers (state) {
      return state.devicePublishers
    }
  },
  mutations: {
    setPublisherDevices (state, devices) {
      state.publisherDevices = devices
    },
    setWorkerDevices (state, devices) {
      state.workerDevices = devices
    },
    devicePublishers (state, devices) {
      state.devicePublishers = devices
    },
    updateDevicePublisher (state, device) {
      let tempDevices = []
      for (let i = 0; i < state.devicePublishers.length; i++) {
        if (state.devicePublishers[i].id === device.id) {
          tempDevices.push(device)
        } else {
          tempDevices.push(state.devicePublishers[i])
        }
      }

      state.devicePublishers = tempDevices
      // change device data
    }
  },
  actions: {
    fetchDevicePublishers (context, devices) {
      context.commit('devicePublishers', devices)
    },
    updateDevicePublisher (context, devices) {
      context.commit('updateDevicePublisher', devices)
    }
  }
}
