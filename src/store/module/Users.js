import {appConfig} from '../../../config/app'
import Vue from 'vue'

export const usersModule = {
  state: {
    usersCollection: {}
  },
  getters: {
    usersCollection (state) {
      return state.usersCollection
    }
  },
  mutations: {
    setUsersCollection (state, usersCollection) {
      state.usersCollection = usersCollection
    }
  },
  actions: {
    fetchUserCollection (context, data) {
      const url = appConfig.URL.user + (data.getParams ? data.getParams : '')

      Vue.http.get(url).then(response => {
        context.commit('setUsersCollection', response.data)
      })
    }
  }
}
