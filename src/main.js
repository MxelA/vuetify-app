// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import router from './router'
import {store} from './store'
import VueResource from 'vue-resource'
import errorHandlerLib from './lib/error_handler'
import authLib from './lib/auth'
import Vue2Filters from 'vue2-filters'

Vue.use(Vuetify)
Vue.use(VueResource)
Vue.use(Vue2Filters)

Vue.component('aria-info', require('./components/area/editPartial/_AreaInfo.vue'))
Vue.component('users-table-list', require('./components/user/partial/_UsersTable.vue'))
Vue.component('areas-table-list', require('./components/user/partial/_AreasTable.vue'))
Vue.component('aria-groups-table-list', require('./components/area/editPartial/_AreaGroups.vue'))
Vue.component('aria-device-worker-table-list', require('./components/area/editPartial/_AreaDeviceWorker.vue'))
Vue.component('aria-device-publisher-table-list', require('./components/area/editPartial/_AreaDevicePublisher.vue'))
Vue.component('aria-config-device-action-table-list', require('./components/area/editPartial/_AreaConfigurationDeviceAction.vue'))
Vue.component('aria-config-action-rule-time', require('./components/area/editPartial/__AreaConfigurActionRuleTime.vue'))
Vue.component('aria-device-workers-control-table-list', require('./components/area/infoPartials/_AreaDeviceWorkersControl.vue'))
Vue.component('aria-device-publisher-data-info-table-list', require('./components/area/infoPartials/_AreaDevicePublisherDataInfo.vue'))

Vue.config.productionTip = false

Vue.http.interceptors.push((request, next) => {
  request.headers.set('Authorization', 'Bearer ' + authLib.getToken())
  request.headers.set('Accept-Language', 'application/json')
  request.headers.set('Content-Type', 'application/json')

  next(function (response) {
    if (response.status !== 200) {
      errorHandlerLib.error(this, response)
    }
  })
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store: store,
  template: '<App/>',
  components: { App }
})
